import React from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  WebView,
  Dimensions,
  Button,
} from 'react-native';

import Board from '../classes/board.js'
let {height, width} = Dimensions.get('window');
const board = new Board(height, width);
export default class TableScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = board;
    }

    componentDidMount() {
        this.setState(board);
        board.start();
    }

    componentWillUnmount() {
    }

    _callBack = ()=>{
        console.log("CALLING",this)
        
    }

    _updatePieces(g) {
        board.selectPiece(g);
        this.setState(board);
    }

    _reset(){
        board.reset();
        this.setState(board);
    }
    
    render(){
        return (
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <View style={styles.tooltipUI}>
                    <Text> {this.state.turn} </Text>
                    <TouchableOpacity style={styles.resetButton} onPress={()=>{this._reset()}}>
                        <Text> RESET </Text>
                    </TouchableOpacity>
                </View>
                

                <View style={styles.boardContainer}>
                    {Object.keys(this.state.row).map((key,index) =>{
                        const column = this.state.row[key].column;
                        return (
                            

                            <View style={styles.row} key={key}>
                                {Object.keys(column).map((colKey)=>{
                                    const square = column[colKey];
                                    const active = column[colKey].highlight;
                                    let Unit;

                                    if(square.cell){
                                        Unit = 
                                            <Text style={{fontSize:30}}>{String.fromCharCode(square.cell.unit)}</Text>

                                        
                                    }

                                    return(
                                        <TouchableOpacity onPress={() => {this._updatePieces(square);}} key={colKey}>

                                            <View style={[(active ? styles.highlight : square.styles), styles.square]} >
                                                
                                                <Text style={styles.labelText}>{square.label} {square.highlight}</Text>
                                            
                                                <View style={{justifyContent: 'flex-end',flex:2,alignItems: 'center'}}> 
                                                    {Unit}
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                })}
                            </View>
                        )
                    })}
                </View>
            </ScrollView>
            
        )
    }
}
const styles = StyleSheet.create({
    contentContainer: {
      flex:1,
      alignItems: 'center',
      justifyContent: 'flex-start',
      height:'80%'
    },
    tooltipUI:{
        width:board.playArea.width,
        borderWidth:1,
        borderColor:'#ddd',
        marginTop:5,
        marginBottom:5,
        justifyContent: 'center',
        fontSize:10,
    },
    resetButton:{
        borderWidth:1
    },
    boardContainer:board.playArea,
    row:{
        flexDirection: 'row',
    },
    square:board.squareArea,
    highlight:{
        backgroundColor:'#68ad74b5',
        borderWidth:5,
        borderColor:'black'
    },
    labelText:{
        fontSize:4,
        margin:.5
    },
    right:{
        justifyContent: 'flex-end',
        textAlign:'right',
    },
    unit:{
        backgroundColor:'transparent',
        fontSize:3
    },
    imageBackground: {
        width:'100%',
        height:'100%',
        borderWidth: 1,
        borderColor: '#d6d7da',
    }
});
