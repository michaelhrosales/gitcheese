import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import {HomeStack,LinksStack,SettingsStack,withNav,TableStack} from './MainTabNavigator';

export default createAppContainer(TableStack);

// export default createAppContainer(createSwitchNavigator({
//   // You could add another route here for authentication.
//   // Read more at https://reactnavigation.org/docs/en/auth-flow.html
//   Main: withNav,
// }));