import {King,Queen,Bishop,Knight,Rook,Pawn} from './piece.js';
import {
    Alert
  } from 'react-native';

class Board{
    constructor(height,width){
        this.players = ['White','Black'];
        this.colors = {
            odd:'#fff',
            even:'rgb(192, 192, 192)'
        };
        this.turn = null;
        this.active = null;
        this.columns = 8;
        this.height = this.width = Math.floor(width-(width*.20)/2);
        this.square = (this.width/this.columns);
        this.boardHeight = this.height + 2;
        this.boardWidth  = this.width + 2;
        this.boardBorder = 1;
        this.rowNames = ['8','7','6','5','4','3','2','1'];
        this.colNames = ['A','B','C','D','E','F','G','H'];
        this.row = {};
        this.pieces = {};
        this.selected = this.players[0];
        this.createPieces(this.players[0]);
        this.createPieces(this.players[1]);    
        this.createRows();
        
        this.updateBoard();
    }

    get playArea(){
        return {
            height: this.boardHeight,
            width:  this.boardWidth,
            borderWidth: this.boardBorder,
            backgroundColor: '#ddd' ,
            borderColor:'black'
        }
    }

    get squareArea(){
        return {
            height:this.square,
            width:this.square,
            borderWidth: .1,
            borderColor: '#d6d7da'
        }
    }

    get selectedColor(){
        return this.selected;
    }

    isSelected(color){
        if(color == this.selected){
            return true;
        }
        return false;
    }

    selectSide(color){
        this.selected = color;
    }

    createRows(){
        this.rowNames.forEach((name,index)=>{
            this.row[index] = {
                column:this.createColumns(name,index),
            }
        });
    }

    createColumns(name,index){
        let columns = {};
        let i = 0;
        while (i < this.columns) {
            let color = this.squareColor(name,index,i);
            let columnName =  `${i}`;
            columns[columnName] = {
                label:`${this.rowNames[(this.columns-1)-index]}${this.colNames[i]}`,
                coords:{
                    codeR:index,
                    codeC:i,
                    row:index,
                    column:i
                },
                styles:{
                    backgroundColor: color
                },
                update: (a)=>{
                    return this.players.map(g =>{
                        const pieces = Object.keys(this.pieces[g].parts).map(x=>{
                            return this.pieces[g].parts[x]
                        });
                        return pieces.find(piece =>{
                            if(piece.row == a.row && piece.column == a.column && !piece.isDead){
                                return piece
                            }
                        });
                    }).find(a => a) || null;
                },
                cell:null,
                highlight:false
            }
            i++;
        }
        return columns;
    }

    createPieces(player){
        this.pieces[player] = {
            parts:{},
            dump:{}
        };
        const parts = {
            'rook-A':new Rook(player,`${player}-${1}-rook`,this.isSelected(player),0),
            'knight-A':new Knight(player,`${player}-${2}-knight`,this.isSelected(player),1),
            'bishop-A':new Bishop(player,`${player}-${3}-bishop`,this.isSelected(player),2),
            'queen':new Queen(player,`${player}-${4}-queen`,this.isSelected(player),3),
            'king':new King(player,`${player}-${5}-king`,this.isSelected(player),4),
            'bishop-B':new Bishop(player,`${player}-${6}-bishop`,this.isSelected(player),5),         
            'knight-B':new Knight(player,`${player}-${7}-knight`,this.isSelected(player),6),       
            'rook-B':new Rook(player,`${player}-${8}-rook`,this.isSelected(player),7),
        };  
        for(let i=0;i<this.columns;i+=1){
            parts[`pawn${i+1}`] = new Pawn(player,`${player}-${i}-pawn`,this.isSelected(player),i);
        }

        this.pieces[player].parts = parts;
        this.pieces[player].selected = this.isSelected(player);
        this.layoutPieces(player);
    }

    layoutPieces(player){
        const layout = this.pieces[player].parts;
        const selected = this.pieces[player].selected;
        const playerRow = {
            pawn:6,
            official:7
        }
        const opponentRow = {
            pawn:1,
            official:0
        }
        
        const setInitialLayout = (piece,coords) =>{
            if(piece.type == 'Pawn'){
                piece.setCoord(coords.pawn,piece.start,true);
            }else{
                piece.setCoord(coords.official,piece.start,true);
            }
        };

        Object.keys(layout).map(g =>{
            switch(selected){
                case true:
                    setInitialLayout(layout[g],playerRow);
                break;
                default:
                    setInitialLayout(layout[g],opponentRow);
                break;
            }
            
        });
    }

    squareColor(row,rowIndex,colIndex){
        const odd = this.colors.odd;
        const even = this.colors.even;
        const colColor = this.selected == this.players[0] ? 1: 0;
        
        switch(rowIndex%2){
            case (colColor):
                switch(colIndex%2){
                    case (1):
                        return odd;
                    default:
                        return even;
                }
                
            default:
                switch(colIndex%2){
                    case (1):
                        return even;
                    default:
                        return odd;
                }
        }
    }

    updateBoard(){
        
        Object.keys(this.row).map((g,i)=>{
            Object.keys(this.row[i].column).map(h=>{
                this.row[i].column[h].cell = this.row[i].column[h].update(this.row[i].column[h].coords) || null;
            });  
        });
    }

    highlightOff(){
        Object.keys(this.row).map((g,i)=>{
            Object.keys(this.row[i].column).map(h=>{
                this.row[i].column[h].highlight = false;
            });  
        })
    }

    movePiece(Cell,Piece){
        Piece.setCoord(Cell.coords.row,Cell.coords.column);
        this.highlightOff();
        this.active = null;
        this.switchTurn();
    }

    holdPiece(Square){
        const Piece = Square.cell;

        if(!this.active){
            this.active = Piece
        }else{
            if(Piece){
                const player = this.active.player;
                
                switch(Piece.player){
                    case (player):
                        //PIECE IS ALLY
                        this.highlightOff();
                        if(this.active.id === Piece.id){
                            this.active = null;
                        }else{
                            this.active = Piece;
                        }
                        
                        
                    break;
    
                    default:
                        //PIECE IS ENEMY
                        const attack = this.doAttack(Square)
                        if(attack){
                            this.movePiece(Square,this.active);
                            this.highlightOff();
                            this.active = null;
                            if(attack.type == 'King'){
                                this.end(attack);
                                return this.active;
                            }
                        }else{
                            
                        }
                    break;
                }
            }else{
                //SELECTED EMPTY CELL
                const moveAble = this.checkMove(Square);
                if(moveAble){
                    // moveable
                    this.movePiece(Square,this.active);
                    this.highlightOff();
                    this.active = Piece;
                }else{
                    // Immovable
                }
            }
            
            
        }
        
        return this.active;
        
    }

    doAttack(Square){
       
        if(Square.highlight == 'Attack'){
            // CAN ATTACK
            
            const dedz = Square.cell.deadPiece(this.end);

            return dedz
        }

        // CAN'T ATTACK
        return false;
    }

    checkMove(Square){
        if(Square.highlight){
            console.log(`I can move here`)
            return true;
        }else if(Square.cell){
            console.log(`square not empty`)
            return false;
        }else{
            console.log(`square empty`);
            this.highlightOff();
            this.active = null;
            return false;
        }
    }

    highlightMovement(){
        const moves = this.active.unitMovement(this.row,this.active.player);
        moves.map(moves=>{
            //check if blocked              
            switch(moves.type){
                case 'Attack':
                    if(this.row[moves.row].column[moves.column].cell){
                        this.row[moves.row].column[moves.column].highlight = moves.type;
                    }
                break;
                case 'Move':  
                default:
                    this.row[moves.row].column[moves.column].highlight = moves.type;
                break;
            }
        });
    }

    selectPiece(select){
       
        if(this.active){
            const active = this.holdPiece(select);
            if(active) this.highlightMovement();
            this.updateBoard(); 
        }else{
            if(select.cell && select.cell.color == this.turn){
                const active = this.holdPiece(select);
                if(active) this.highlightMovement();
                this.updateBoard(); 
            }
        }
        
       
    }

    start(){
        this.turn = this.players[0];
    }

    end(loser){
        const selected = loser.color !== this.player ? true: false;
        const title = selected ? 'Winner':'Loser';
        const message = selected ? 'GG Easy':'Pfft Weak'

        Alert.alert(
            title,
            message,
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'OK', onPress: () => {
                console.log("AVENGERS END GAME",this);
                this.reset();
              }},
            ],
            {cancelable: false},
          );
    }

    reset(){
        this.active = null;
        this.row = {};
        this.pieces = {};
        this.selected = this.players[0];
        this.turn = this.players[0];
        this.createPieces(this.players[0]);
        this.createPieces(this.players[1]);    
        this.createRows();
        this.updateBoard();
    }

    switchTurn(){
        this.turn =  this.players.find(a =>a !== this.turn);
    }
}


export default Board;