class Piece{
    constructor(type,color,id,isSelected,model,start){
        this.type = type;
        this.color = color;
        this.id = id;
        this.row = null;
        this.column = null;
        this.player = isSelected;
        this.untouched = true;
        this.model = model;
        this.points = 0;
        this.moveType = [
            'Move',
            'Attack',
            'Initial'
        ];
        this.isDead = false;
        this.start = start;
        this.limit = 8;
        this.MoveLimit = this.limit;

    }

    setCoord(row,column,initial){
        this.row = row;
        this.column = column;
        this.untouched = initial || false;
    }

    getCoord(){
        const dirs = {
            row: this.row,
            column: this.column,
            type:'Initial'
        };
        return dirs
    }

    checkMove(){
        console.log(`I am a ${this.type} and my move set is:`);
        console.log(`I am in row: ${this.row} and in column: ${this.column}`);
        console.log(`I am ${this.player? 'the player':'an opponent'}`)
        return;
    }

    deadPiece(){
        this.isDead = true;
        return {
            type:this.type,
            color:this.color
        }
    }
    
    unitMovement(){
        //@override
        return [];
    }

    specialMove(){
        //@override
        return [];
    }

    getHorizontal(Coord,Board,HeldPiece,MoveLimit){
        const moves = [];
        const moveRight = () =>{
            let moveLimit = MoveLimit;
            const container = [];
            let next = Coord.column+1;
            while(next < this.limit){
                const move = {
                    row:Coord.row,
                    column:next,
                    type:this.moveType[0]
                };
                const Cell =Board[Coord.row].column[next].cell;
                if(Cell){
                    switch(Cell.player){
                        case (HeldPiece):
                            return container;
                        default:
                            move.type = this.moveType[1];
                            container.push(move);
                            return container;
                    }
                }

                container.push(move);
                next+=1;


                moveLimit-=1;
                if(moveLimit == 0) return container;

            }
            return container;
        };

        const moveLeft = () =>{
            let moveLimit = MoveLimit;
            const container = [];
            let next = Coord.column-1;
            while(next >= 0){
                const move = {
                    row:Coord.row,
                    column:next,
                    type:this.moveType[0]
                };
                const Cell =Board[Coord.row].column[next].cell;
                if(Cell){
                    switch(Cell.player){
                        case (HeldPiece):
                            return container;
                        default:
                            move.type = this.moveType[1];
                            container.push(move);
                            return container;
                    }
                }

                container.push(move);
                next-=1;

                moveLimit-=1;
                if(moveLimit == 0) return container;
            }
            return container;
        };

        return moves.concat(moveLeft()).concat(moveRight());
    }

    getVertical(Coord,Board,HeldPiece,MoveLimit){
        const moves = [];

        const moveDown = () =>{
            let moveLimit = MoveLimit;
            const container = [];
            let next = Coord.row+1;
            while(next < this.limit){
                const move = {
                    row:next,
                    column:Coord.column,
                    type:this.moveType[0]
                };
                const Cell =Board[next].column[Coord.column].cell;
                if(Cell){
                    switch(Cell.player){
                        case (HeldPiece):
                            return container;
                        default:
                            move.type = this.moveType[1];
                            container.push(move);
                            return container;
                    }
                }

                container.push(move);
                next+=1;

                moveLimit-=1;
                if(moveLimit == 0) return container;
            }
            return container;
        };

        const moveUp = () =>{
            let moveLimit = MoveLimit;
            const container = [];
            let next = Coord.row-1;
            while(next >= 0){
                const move = {
                    row:next,
                    column:Coord.column,
                    type:this.moveType[0]
                };
                const Cell =Board[next].column[Coord.column].cell;
                if(Cell){
                    switch(Cell.player){
                        case (HeldPiece):
                            return container;
                        default:
                            move.type = this.moveType[1];
                            container.push(move);
                            return container;
                    }
                }

                container.push(move);
                next-=1;

                moveLimit-=1;
                if(moveLimit == 0) return container;
            }
            return container;
        };
        return moves.concat(moveUp()).concat(moveDown());
    }

    getDiagonal(Coord,Board,HeldPiece,MoveLimit){
        const moves = [];
        const northEast = () =>{
            let moveLimit = MoveLimit;
            const container = [];
            let next = Coord.row-1;
            let nextCol = Coord.column+1;

            
            while(next >= 0 && nextCol < this.limit){
                const move = {
                    row:next,
                    column:nextCol,
                    type:this.moveType[0]
                };
                const Cell = Board[next].column[nextCol].cell;
                
                if(Cell){
                    switch(Cell.player){
                        case (HeldPiece):
                            return container;
                        default:
                            move.type = this.moveType[1];
                            container.push(move);
                            return container;
                    }
                }

                container.push(move);
                nextCol+=1;
                next-=1;

                moveLimit-=1;
                if(moveLimit == 0) return container;
            }
            return container;
        };


        const southEast = () =>{
            let moveLimit = MoveLimit;
            const container = [];
            let next = Coord.row+1;
            let nextCol = Coord.column-1;

            
            while(next < this.limit && nextCol >= 0){
                const move = {
                    row:next,
                    column:nextCol,
                    type:this.moveType[0]
                };
                const Cell = Board[next].column[nextCol].cell;
                
                if(Cell){
                    switch(Cell.player){
                        case (HeldPiece):
                            return container;
                        default:
                            move.type = this.moveType[1];
                            container.push(move);
                            return container;
                    }
                }

                container.push(move);
                nextCol-=1;
                next+=1;

                moveLimit-=1;
                if(moveLimit == 0) return container;
            }
            return container;
        };



        const northWest = () =>{
            let moveLimit = MoveLimit;
            const container = [];
            let next = Coord.row-1;
            let nextCol = Coord.column-1;

            
            while(next >= 0 && nextCol >= 0){
                const move = {
                    row:next,
                    column:nextCol,
                    type:this.moveType[0]
                };
                const Cell = Board[next].column[nextCol].cell;
                
                if(Cell){
                    switch(Cell.player){
                        case (HeldPiece):
                            return container;
                        default:
                            move.type = this.moveType[1];
                            container.push(move);
                            return container;
                    }
                }

                container.push(move);
                nextCol-=1;
                next-=1;

                moveLimit-=1;
                if(moveLimit == 0) return container;
            }
            return container;
        };

        const southWest = () =>{
            let moveLimit = MoveLimit;
            const container = [];
            let next = Coord.row+1;
            let nextCol = Coord.column+1;

            
            while(next < this.limit && nextCol < this.limit){
                const move = {
                    row:next,
                    column:nextCol,
                    type:this.moveType[0]
                };
                const Cell = Board[next].column[nextCol].cell;
                
                if(Cell){
                    switch(Cell.player){
                        case (HeldPiece):
                            return container;
                        default:
                            move.type = this.moveType[1];
                            container.push(move);
                            return container;
                    }
                }

                container.push(move);
                nextCol+=1;
                next+=1;

                moveLimit-=1;
                if(moveLimit == 0) return container;
            }
            
            return container;
        };

        return moves.concat(northEast()).concat(southEast()).concat(northWest()).concat(southWest());
    }


    getUnit(color){
        return this.model[color];
    }
    
}

export class King extends Piece{
    constructor(color,id,isSelected,start) {
        const player = {
            White:9812,
            Black:9818
        }
        super('King',color,id,isSelected,player,start);
        this.unit = super.getUnit(color);
        this.moveLimit = 1;
    }

    unitMovement(Board,HeldPiece){
        const moves = [];
        const coord = super.getCoord();
        coord.type = this.moveType[2]; //INITIAL

        moves.push(coord);
        
        return moves.concat(super.getVertical(coord,Board,HeldPiece,this.moveLimit))
                    .concat(super.getHorizontal(coord,Board,HeldPiece,this.moveLimit))
                    .concat(super.getDiagonal(coord,Board,HeldPiece,this.moveLimit));
    }


    checkMove(){
        return super.checkMove();
    }
}

export class Queen extends Piece{
    constructor(color,id,isSelected,start) {
        const player = {
            White:9813,
            Black:9819
        }
        super('Queen',color,id,isSelected,player,start);
        this.unit = super.getUnit(color);

    }

    unitMovement(Board,HeldPiece){
        const moves = [];
        const coord = super.getCoord();
        coord.type = this.moveType[2]; //INITIAL

        moves.push(coord);
        
        return moves.concat(super.getVertical(coord,Board,HeldPiece,this.moveLimit))
                    .concat(super.getHorizontal(coord,Board,HeldPiece,this.moveLimit))
                    .concat(super.getDiagonal(coord,Board,HeldPiece,this.moveLimit));
    }


    checkMove(){
        return super.checkMove();
    }

}

export class Bishop extends Piece{
    constructor(color,id,isSelected,start) {
        const player = {
            White:9815,
            Black:9821
        }
        super('Bishop',color,id,isSelected,player,start);
        this.unit = super.getUnit(color);
    }

    unitMovement(Board,HeldPiece){
        const moves = [];
        const coord = super.getCoord();
        coord.type = this.moveType[2]; //INITIAL

        moves.push(coord);
        return moves.concat(super.getDiagonal(coord,Board,HeldPiece,this.moveLimit));
    }

    checkMove(){
        return super.checkMove();
    }
}

export class Knight extends Piece{
    constructor(color,id,isSelected,start) {
        const player = {
            White:9816,
            Black:9822
        }
        super('Knight',color,id,isSelected,player,start); 
        this.unit = super.getUnit(color);
    }

    unitMovement(Board,HeldPiece){
        const moves = [];
        const coord = super.getCoord();
        coord.type = this.moveType[2]; //INITIAL

        moves.push(coord);
        
        const north = () =>{
            const container = [];
            let up = coord.row-2;
            let left = coord.column-1;
            let right = coord.column+1;
            

            if(up >= 0){
                if(left >= 0){
                    const CellLeft = Board[up].column[left].cell;
                    const move = {
                        row:up,
                        column:left,
                        type:this.moveType[0]
                    };

                    if(CellLeft){
                        switch(CellLeft.player){
                            case (HeldPiece):
                            break;
                            default:
                                move.type = this.moveType[1];
                                container.push(move);
                        }
                    }else{
                        container.push(move);
                    }                }
    
                if(right < this.limit){
                    const CellRight = Board[up].column[right].cell;
                    const move = {
                        row:up,
                        column:right,
                        type:this.moveType[0]
                    };

                    if(CellRight){
                        switch(CellRight.player){
                            case (HeldPiece):
                            break;
                            default:
                                move.type = this.moveType[1];
                                container.push(move);
                        }
                    }else{
                        container.push(move);
                    }
                }
            }
            return container;
        };

        const south = () =>{
            const container = [];
            let down = coord.row+2;
            let left = coord.column-1;
            let right = coord.column+1;
            

            if(down < this.limit){
                if(left >= 0){
                    const CellLeft = Board[down].column[left].cell;
                    const move = {
                        row:down,
                        column:left,
                        type:this.moveType[0]
                    };

                    if(CellLeft){
                        switch(CellLeft.player){
                            case (HeldPiece):
                            break;
                            default:
                                move.type = this.moveType[1];
                                container.push(move);
                        }
                    }else{
                        container.push(move);
                    }                }
    
                if(right < this.limit){
                    const CellRight = Board[down].column[right].cell;
                    const move = {
                        row:down,
                        column:right,
                        type:this.moveType[0]
                    };

                    if(CellRight){
                        switch(CellRight.player){
                            case (HeldPiece):
                            break;
                            default:
                                move.type = this.moveType[1];
                                container.push(move);
                        }
                    }else{
                        container.push(move);
                    }
                }
            }
            return container;
        };

        const east = () =>{
            const container = [];
            let up = coord.row-1;
            let right = coord.column+2;
            let down = coord.row+1;
            
            if(right < this.limit){
                if(up >= 0){
                    const CellUp = Board[up].column[right].cell;
                    const move = {
                        row:up,
                        column:right,
                        type:this.moveType[0]
                    };

                    if(CellUp){
                        switch(CellUp.player){
                            case (HeldPiece):
                            break;
                            default:
                                move.type = this.moveType[1];
                                container.push(move);
                        }
                    }else{
                        container.push(move);
                    }
                }
    
                if(down < this.limit){
                    const CellDown = Board[down].column[right].cell;
                    const move = {
                        row:down,
                        column:right,
                        type:this.moveType[0]
                    };

                    if(CellDown){
                        switch(CellDown.player){
                            case (HeldPiece):
                            break;
                            default:
                                move.type = this.moveType[1];
                                container.push(move);
                        }
                    }else{
                        container.push(move);
                    }
                    
                }
            }
            return container;
        };

        const west = () =>{
            const container = [];
            let up = coord.row-1;
            let left = coord.column-2;
            let down = coord.row+1;
            
            if(left >= 0){
                if(up >= 0){
                    const CellUp = Board[up].column[left].cell;
                    const move = {
                        row:up,
                        column:left,
                        type:this.moveType[0]
                    };

                    if(CellUp){
                        switch(CellUp.player){
                            case (HeldPiece):
                            break;
                            default:
                                move.type = this.moveType[1];
                                container.push(move);
                        }
                    }else{
                        container.push(move);
                    }
                }
    
                if(down < this.limit){
                    const CellDown = Board[down].column[left].cell;
                    const move = {
                        row:down,
                        column:left,
                        type:this.moveType[0]
                    };

                    if(CellDown){
                        switch(CellDown.player){
                            case (HeldPiece):
                            break;
                            default:
                                move.type = this.moveType[1];
                                container.push(move);
                        }
                    }else{
                        container.push(move);
                    }
                    
                }
            }
            return container;
        };

        return moves.concat(north())
                    .concat(east())
                    .concat(west())
                    .concat(south());
    }

    checkMove(){
        return super.checkMove();
    }
}

export class Rook extends Piece{
    constructor(color,id,isSelected,start) {
        const player = {
            White:9814,
            Black:9820
        }
        super('Rook',color,id,isSelected,player,start);
        this.unit = super.getUnit(color);
        this.moveLimit = 8;
    }

    unitMovement(Board,HeldPiece){
        const moves = [];
        const coord = super.getCoord();
        coord.type = this.moveType[2]; //INITIAL

        moves.push(coord);
        
        return moves.concat(super.getVertical(coord,Board,HeldPiece,this.moveLimit)).concat(super.getHorizontal(coord,Board,HeldPiece,this.moveLimit));
    }

    checkMove(){
        return super.checkMove();
    }
}

export class Pawn extends Piece{
    
    
    constructor(color,id,isSelected,start) {
        const player = {
            White:9817,
            Black:9823
        }
        super('Pawn',color,id,isSelected,player,start);
        this.unit = super.getUnit(color);
       
    }

    unitMovement(Board,HeldPiece){
        const direction = this.player ? -1:1; //row movement -1 => upwards 1 => downwards
        //diagonal
       
        const moves = [];
        const coord = super.getCoord();
        coord.type = this.moveType[2];
        const left = coord.column-1;
        const right= coord.column+1;
        const limit = coord.row+(direction);
        
        moves.push(coord);
        if(limit < this.limit && limit >= 0){
            
            
            let pace = this.untouched ? 2:1;
            let plus = 1;
            while(pace){
                const row = coord.row+(plus*direction);
                const cell = Board[row].column[coord.column].cell;
                const move = {
                    row:row,
                    column:coord.column,
                    type:this.moveType[0]
                }
                
                if(left>=0){
                    const moveLeft = {
                        row:limit,
                        column:left,
                        type:this.moveType[1]
                    }

                    const aLeft  = Board[limit].column[coord.column-1].cell;
                    
                    if(aLeft){
                        switch(aLeft.player){
                            case (HeldPiece):
                            break;
                            default:
                                move.type = this.moveType[1];
                                moves.push(moveLeft);
                                break;
                        }
                    }
                    
                    
                }

                if(right < this.limit){
                    const moveRight = {
                        row:limit,
                        column:right,
                        type:this.moveType[1]
                    }
                    
                    const aRight  = Board[limit].column[coord.column+1].cell;
                    
                    if(aRight){
                        switch(aRight.player){
                            case (HeldPiece):
                            break;
                            default:
                                move.type = this.moveType[1];
                                moves.push(moveRight);
                                break;
                        }
                    }
                    
                }
                
                if(!cell){
                    move.type = this.moveType[0]
                    moves.push(move);
                }
                plus+=1;
                pace-=1;
                
                
            }
            return moves;
        }

        
        
        
    }

    

    checkMove(){
        return super.checkMove();
    }
}

