class Piece{
    constructor(type,color,id,isSelected,model){
        this.type = type;
        this.color = color;
        this.id = id;
        this.row = null;
        this.column = null;
        this.player = isSelected;
        this.untouched = true;
        this.model = model;
        this.moveSet = [];
        this.points = 0;
    }

    setCoord(row,column,initial){
        this.row = row;
        this.column = column;
        this.untouched = initial || false;
    }

    getCoord(){
        const dirs = {
            row: this.row,
            column: this.column
        };
        return dirs
    }

    checkMove(){
        console.log(`I am a ${this.type} and my move set is:`);
        console.log(`I am in row: ${this.row} and in column: ${this.column}`);
        console.log(`I am ${this.player? 'the player':'an opponent'}`)
        return;
    }
    

    unitMovement(){
        //@override
        return "Unaassigned";
    }

    getUnit(color){
        return this.model[color];
    }
    
}

export class King extends Piece{
    constructor(color,id,isSelected) {
        const player = {
            White:9812,
            Black:9818
        }
        super('King',color,id,isSelected,player);
        this.unit = super.getUnit(color);

    }
    

    checkMove(){
        return super.checkMove();
    }
}

export class Queen extends Piece{
    constructor(color,id,isSelected) {
        const player = {
            White:9813,
            Black:9819
        }
        super('Queen',color,id,isSelected,player);
        this.unit = super.getUnit(color);

    }


    checkMove(){
        return super.checkMove();
    }

}

export class Bishop extends Piece{
    constructor(color,id,isSelected) {
        const player = {
            White:9815,
            Black:9821
        }
        super('Bishop',color,id,isSelected,player);
        this.unit = super.getUnit(color);
    }

    checkMove(){
        return super.checkMove();
    }
}

export class Knight extends Piece{
    constructor(color,id,isSelected) {
        const player = {
            White:9816,
            Black:9822
        }
        super('Knight',color,id,isSelected,player); 
        this.unit = super.getUnit(color);
    }

    checkMove(){
        return super.checkMove();
    }
}

export class Rook extends Piece{
    constructor(color,id,isSelected) {
        const player = {
            White:9814,
            Black:9820
        }
        super('Rook',color,id,isSelected,player);
        this.unit = super.getUnit(color);
    }

    checkMove(){
        return super.checkMove();
    }
}

export class Pawn extends Piece{
    
    
    constructor(color,id,isSelected) {
        const player = {
            White:9817,
            Black:9823
        }
        super('Pawn',color,id,isSelected,player);
        this.unit = super.getUnit(color);

       
    }

    unitMovement(){
        const direction = this.player ? -1:1; //row movement -1 => upwards 1 => downwards
        
        //diagonal
        // 1 
        // 2 first move


        return super.getCoord();
    }

    checkMove(){
        return super.checkMove();
    }
}

