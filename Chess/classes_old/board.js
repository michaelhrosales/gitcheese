import {King,Queen,Bishop,Knight,Rook,Pawn} from './piece.js';


class Board{
    constructor(height,width){
        this.players = ['White','Black'];
        this.colors = {
            odd:'#fff',
            even:'rgb(192, 192, 192)'
        };
        this.columns = 8;
        this.height = this.width = Math.floor(width-(width*.20)/2);
        this.square = (this.width/this.columns);
        this.boardHeight = this.height + 2;
        this.boardWidth  = this.width + 2;
        this.boardBorder = 1;
        this.colNames = ['1','2','3','4','5','6','7','8'].reverse();
        this.rowNames = ['A','B','C','D','E','F','G','H'];
        this.row = {};
        this.selected = this.players[0];
        this.createRows();

    }

    get updateAbles(){
        
    }


    get playArea(){
        return {
            height: this.boardHeight,
            width:  this.boardWidth,
            borderWidth: this.boardBorder,
            backgroundColor: '#ddd' ,
            borderColor:'black'
        }
    }

    get squareArea(){
        return {
            height:this.square,
            width:this.square,
            borderWidth: .1,
            borderColor: '#d6d7da'
        }
    }

    get selectedColor(){
        return this.selected;
    }

    isSelected(color){
        if(color == this.selected){
            return true;
        }

        return false;
    }
    

    selectSide(color){
        this.selected = color;
    }

    createRows(){
        this.rowNames.forEach((name,index)=>{
            this.row[index] = this.createColumns(name,index)
        });
        this.createPieces(this.players[0]);
        this.createPieces(this.players[1]);    
    }

    createColumns(name,index){
             
        let columns = {};
        let i = 0;
        while (i < this.columns) {
            let color = this.squareColor(name,index,i);
            let columnName =  `${i}`;
            columns[columnName] = {
                label:`${this.rowNames[i]}${this.colNames[index]}`,
                styles:{
                    backgroundColor: color
                },
                getOccupant:(a)=>{
                    this.getOccupant(a);
                }
            }
            i++;
        }
        return columns;
    }
    getOccupant(piece){
        if(!piece) return;
        const move = piece.unitMovement();
        this.row[move.row][move.column] = piece;
        this.row[move.row-2][move.column] = piece;
        // console.log(this.row[move.row][move.column],"<======");
        // console.log(this.row[move.row-1][move.column],"<======");
        // console.log(this.row[move.row-2][move.column],"<======");
        // setTimeout(()=>{
        return piece;
        // },3000)
    }



    createPieces(player){
        this.pieces = {};
        this.pieces[player] = {
            parts:{}
        };
        const parts = {
            'rook-A':new Rook(player,0,this.isSelected(player)),
            'knight-A':new Knight(player,1,this.isSelected(player)),
            'bishop-A':new Bishop(player,2,this.isSelected(player)),
            'queen':new Queen(player,3,this.isSelected(player)),
            'king':new King(player,4,this.isSelected(player)),
            'bishop-B':new Bishop(player,5,this.isSelected(player)),         
            'knight-B':new Knight(player,6,this.isSelected(player)),       
            'rook-B':new Rook(player,7,this.isSelected(player)),
        };  
        for(let i=0;i<this.columns;i+=1){
            parts[`pawn${i+1}`] = new Pawn(player,i,this.isSelected(player));
        }

        this.pieces[player].parts = parts;
        this.pieces[player].selected = this.isSelected(player);
        this.pieces[player].dump = {};

        this.layoutPieces(player);
    }

    layoutPieces(player){
        const layout = this.pieces[player].parts;
        const selected = this.pieces[player].selected;
        const playerRow = {
            pawn:6,
            official:7
        }
        const opponentRow = {
            pawn:1,
            official:0
        }
        
        const setInitialLayout = (piece,coords) =>{
            if(piece.type == 'Pawn'){
                this.row[coords.pawn][piece.id].cell = piece;
                piece.setCoord(coords.pawn,piece.id,true);
            }else{
                this.row[coords.official][piece.id].cell = piece;
                piece.setCoord(coords.official,piece.id,true);
            }
        };

        Object.keys(layout).map(g =>{
            switch(selected){
                case true:
                    setInitialLayout(layout[g],playerRow);
                break;
                default:
                    setInitialLayout(layout[g],opponentRow);
                break;
            }
            
        });
    }

    updateBoard(){
        const [a,b] = [this.players[0],this.players[1]];
        console.log(this.pieces)
    }

    squareColor(row,rowIndex,colIndex){
        const odd = this.colors.odd;
        const even = this.colors.even;
        switch(rowIndex%2){
            case (1):
                switch(colIndex%2){
                    case (1):
                        return odd;
                    default:
                        return even;
                }
                
            default:
                switch(colIndex%2){
                    case (1):
                        return even;
                    default:
                        return odd;
                }
        }
    }
}


export default Board;