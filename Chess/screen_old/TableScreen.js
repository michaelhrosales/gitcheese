import React from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  WebView,
  Dimensions,
} from 'react-native';

import Board from '../classes/board.js'
let {height, width} = Dimensions.get('window');
const board = new Board(height, width);
export default class TableScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = board;
    }

    componentDidMount() {
        // this.setState(board);
        // console.log(this.state,'component Did mount')
    }

    componentWillUnmount() {
    }

    _updatePieces(g) {
        console.log(g.cell.row);
        console.log(g.cell.column);
        this.state.row[g.cell.row][g.cell.column].cell.setCoord(g.cell.row-2,g.cell.column);
        this.state.updateBoard();
        this.setState({state:board})
    }
    
    render(){
        return (
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <View style={styles.boardContainer}>
                    {Object.keys(this.state.row).map((key,index) =>{
                        const column = this.state.row[key];
                        return (
                            <View style={styles.row} key={key}>
                                {Object.keys(column).map((colKey,index)=>{
                                    const square = column[colKey];
                                    let Unit;

                                    if(square.cell){
                                        Unit = 
                                        <TouchableOpacity onPress={() => {
                                            
                                            this._updatePieces(square);
                                            
                                            }
                                            }>
                                            <Text style={{fontSize:30}}>{String.fromCharCode(square.cell.unit)}</Text>
                                            <Text style={{fontSize:5}}>{square.cell.row}:{square.cell.column}</Text>
                                        </TouchableOpacity>
                                    }else{
                                        Unit = <Text> EMPTY </Text>
                                    }

                                return(
                                        <View style={[square.styles, styles.square]} key={colKey}>
                                            
                                            <Text style={styles.labelText}>{square.label} </Text>
                                            
                                            <View style={{justifyContent: 'flex-end',flex:2,alignItems: 'center'}}> 
                                                {Unit}
                                            </View>

                                            <View style={{justifyContent: 'flex-end',flex:.5}}>
                                                <Text style={[styles.labelText,styles.right]}>
                                                    {key}-{colKey}
                                                </Text>

                                                
                                            </View> 
                                        </View>
                                )
                                })}
                            </View>
                        )
                    })}
                </View>
            </ScrollView>
            
        )
    }
}
const styles = StyleSheet.create({
    contentContainer: {
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
      height:'80%'
    },
    boardContainer:board.playArea,
    row:{
        flexDirection: 'row',
    },
    square:board.squareArea,
    labelText:{
        fontSize:4,
        margin:.5
    },
    right:{
        justifyContent: 'flex-end',
        textAlign:'right',
    },
    unit:{
        backgroundColor:'transparent',
        fontSize:3
    },
    imageBackground: {
        width:'100%',
        height:'100%',
        borderWidth: 1,
        borderColor: '#d6d7da',
    }
});
